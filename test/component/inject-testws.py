#!/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gSystem.Load("libRooFit")
ROOT.gSystem.Load("libHistFactory")
ROOT.gSystem.Load("libRooLagrangianMorphing.so")

def main():
    import ROOT
    from math import sqrt
    infile = ROOT.TFile.Open("morphing.root")
    ws = infile.Get("morphing")
    pdf = ws.pdf("simPdf")
    cat = ws.cat("channelCat")
    cat.setIndex(0)
    cats = ROOT.RooArgSet(cat)
    asimov = ws.data("asimovData_even")
    asimov.Print()

    ws.Print("t")
    
    func = ws.function("signal_SR_morphed")
    cosa = ws.var("cosa")
    var = ws.var("obs_x_SR")
    mu = ws.var("mu")
    
    canvas = ROOT.TCanvas("histograms")
    plot = ROOT.RooPlot(var,var.getMin(),var.getMax(),var.numBins())
    asimov.plotOn(plot,ROOT.RooFit.Slice(cat,"SR"))
    
    if cosa: cosa.setVal(0.)
    pdf.plotOn(plot,ROOT.RooFit.Slice(cat,"SR"),ROOT.RooFit.ProjWData(cats,asimov),ROOT.RooFit.LineColor(ROOT.kBlue))
    if func:
        even = func.createTH1("even")
        even.SetLineColor(ROOT.kBlue)
        even.Draw()
    
    if cosa: cosa.setVal(1.)
    pdf.plotOn(plot,ROOT.RooFit.Slice(cat,"SR"),ROOT.RooFit.ProjWData(cats,asimov),ROOT.RooFit.LineColor(ROOT.kRed))
    if func:
        odd = func.createTH1("odd")
        odd.SetLineColor(ROOT.kRed)    
        odd.Draw("SAME")
    
    if cosa: cosa.setVal(1./sqrt(2.))
    pdf.plotOn(plot,ROOT.RooFit.Slice(cat,"SR"),ROOT.RooFit.ProjWData(cats,asimov),ROOT.RooFit.LineColor(ROOT.kGreen))
    if func:
        mix = func.createTH1("mix")
        mix.SetLineColor(ROOT.kGreen)    
        mix.Draw("SAME")
        
    canvas.SaveAs("histograms.pdf")

    canvas = ROOT.TCanvas("plot")
    plot.Draw()
    canvas.SaveAs("plot.pdf","pdf")

    pdf.fitTo(asimov,ROOT.RooFit.SumW2Error(False))

    pdf.Print("t")
    
if __name__ == "__main__":
    main()
    
