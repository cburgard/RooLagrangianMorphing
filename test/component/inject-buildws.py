#!/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gSystem.Load("libRooFit")
ROOT.gSystem.Load("libHistFactory")
ROOT.gSystem.Load("libRooLagrangianMorphing.so")

_noDelete = []
def save(obj):
    _noDelete.append(obj)

def makeHist():
    from math import pi
    hist = ROOT.TH1F("myVar","myVar",13,-pi,pi)
    hist.SetDirectory(0)
    return hist
    
func = ROOT.TF1("sinus","1+sin(x+[0])")
unif = ROOT.TF1("1","1")

def makeSignalHist(shift):
    func.SetParameter(0,shift)
    h = makeHist()
    h.FillRandom("sinus",10000)
    h.Scale(0.01)    
    return h

def makeParamHist(params):
    n = len(params)
    ph = ROOT.TH1F("param_card","param_card",n,0,n)
    ph.SetDirectory(0)
    i = 1
    for p in sorted(params.keys()):
        ph.GetXaxis().SetBinLabel(i,p)
        ph.SetBinContent(i,params[p])
        i = i+1
    return ph

def makeSignalSample(name,pval,shift):
    h  = makeSignalHist(shift)
    p  = makeParamHist({"cosa":pval})
    f = ROOT.TFolder(name,name)
    f.Add(h)
    f.Add(p)
    return f
    
    
def makeMorphingInputs():
    from math import sqrt,pi
    histograms = ROOT.TDirectory("histograms","histograms")
    histograms.Add(makeSignalSample("sm",0,pi/2))
    histograms.Add(makeSignalSample("bsm",1,-pi/2))
    histograms.Add(makeSignalSample("mix",1./sqrt(2),0.))    
    return histograms

def makeMorphingFunction():
    cosa = ROOT.RooRealVar("cosa","cosa",0.,0.,1.)
    const = ROOT.RooRealVar("gConst","gConst",1.,1.,1.)
    const.setConstant(True)
    args = ROOT.RooArgList()
    args.add(cosa)
    sina = ROOT.RooFormulaVar("sina","sina","sqrt(1-cosa*cosa)",args)

    prod = ROOT.RooArgList()
    prod.add(cosa)
    prod.add(sina)

    dec = ROOT.RooArgList()
    dec.add(const)

    save(cosa)
    save(sina)
    save(const)

    name = "signal_SR_morphed"
    morphfunc = ROOT.RooLagrangianMorphFunc(name,name,"","obs_x_SR",prod,dec,"myVar")
    return morphfunc

def makeWorkspace(sighist,bkghist):

    infname = "hist_tmp.root"
    infile = ROOT.TFile.Open(infname,"RECREATE")
    sigclone = sighist.Clone()
    sigclone.SetName("signal")
    sigclone.SetDirectory(infile)
    bkgclone = bkghist.Clone()
    bkgclone.SetName("background")
    bkgclone.SetDirectory(infile)
    infile.Write()
    infile.Close()
    
    # Create the measurement
    meas = ROOT.RooStats.HistFactory.Measurement("morphing", "morphing")
 
    meas.SetPOI( "mu" )
    meas.AddConstantParam("Lumi")
    meas.AddConstantParam("alpha_syst1")
 
    meas.SetLumi( 1.0 )
    meas.SetLumiRelErr( 0.10 )
    meas.SetExportOnly( False )
 
    # Create a channel
 
    chan = ROOT.RooStats.HistFactory.Channel( "SR" )
    chan.SetStatErrorConfig( 0.05, "Poisson" )
 
 
    # Create the signal sample
    signal = ROOT.RooStats.HistFactory.Sample( "signal", "signal", infname )
    signal.AddNormFactor( "mu", 1, 0, 3 )
    chan.AddSample( signal )
 
    # Background 
    background = ROOT.RooStats.HistFactory.Sample( "background", "background", infname )
    chan.AddSample( background )
 
    meas.AddChannel( chan )
 
    # Collect the histograms from their files,
    # print some output, 
    meas.CollectHistograms()
 
    # Now, do the measurement
    ws = ROOT.RooStats.HistFactory.MakeModelAndMeasurementFast( meas );
    return ws


def main():
    signal_histograms = makeMorphingInputs()
    signal_histograms.cd()
    morphfunc = makeMorphingFunction()
    bw = morphfunc.getBinWidth()
    sumfunc = morphfunc.getFunc()
    # needed to avoid double-counting the bin width in histfactory workspaces
    bw.SetName("binWidth_dummy")
    bw.setVal(1.) 

    sighist = signal_histograms.Get("sm").FindObject("myVar")
    bkghist = makeHist()
    bkghist.FillRandom("1",10000)
    bkghist.Scale(0.000001)
    ws = makeWorkspace(sighist,bkghist)
    ws.SetName("morphing")
    ws.SetTitle("RooLagrangianMorphing example")

    morphfunc.insert(ws)
    ROOT.RooLagrangianMorphing.importToWorkspace(ws,sumfunc)
    
    model = ws.obj("ModelConfig")
    pdf = model.GetPdf()

    # by switching between the following two lines, the factory or internal PDF can be exchanged in the workspace
    newpdf = ws.factory("EDIT::{:s}({:s},{:s}={:s})".format(pdf.GetName(),pdf.GetName(),"signal_SR_nominal",morphfunc.GetName())) # use the factory class
#    newpdf = ws.factory("EDIT::{:s}({:s},{:s}={:s})".format(pdf.GetName(),pdf.GetName(),"signal_SR_nominal",sumfunc.GetName())) # use the internal PDF
    model.SetParametersOfInterest("mu,cosa")

    newws = ROOT.RooLagrangianMorphing.makeCleanWorkspace(ws)
    newmodel = newws.obj("ModelConfig")

    cosa = newws.var("cosa")
    if cosa: cosa.setVal(0.)
    even = ROOT.RooStats.AsymptoticCalculator.MakeAsimovData(newmodel,newws.allVars(),newmodel.GetGlobalObservables());
    even.SetName("asimovData_even")
    ROOT.RooLagrangianMorphing.importToWorkspace(newws,even)
    
    if cosa: cosa.setVal(1.)
    odd = ROOT.RooStats.AsymptoticCalculator.MakeAsimovData(newmodel,newws.allVars(),newmodel.GetGlobalObservables());
    odd.SetName("asimovData_odd")
    ROOT.RooLagrangianMorphing.importToWorkspace(newws,odd)

    from math import sqrt
    if cosa: cosa.setVal(1./sqrt(2))
    mixed = ROOT.RooStats.AsymptoticCalculator.MakeAsimovData(newmodel,newws.allVars(),newmodel.GetGlobalObservables());        
    mixed.SetName("asimovData_mixed")
    ROOT.RooLagrangianMorphing.importToWorkspace(newws,mixed)
    
    newws.writeToFile("morphing.root")
    
if __name__ == "__main__":
    main()
    
