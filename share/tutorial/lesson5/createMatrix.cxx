// morphing
#include "RooLagrangianMorphing/RooLagrangianMorphing.h"

// RooFit
#include "RooStringVar.h"

int main(int argc, const char* argv[]){

  // define process identifier, input file and observable
  std::string identifier("vbfWW"); // avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  std::string infilename("input/vbfhwwlvlv_3d.root"); // give the input file name here
  std::string observable("twoSelJets/dphijj"); // name of the observable to be used (histogram name)

  // these are the names of the input samples
  std::vector<std::string> samplelist = {"kAwwkHwwkSM0","kAwwkHwwkSM1","kAwwkHwwkSM10","","kAwwkHwwkSM11","kAwwkHwwkSM12","kAwwkHwwkSM13","kAwwkHwwkSM2","kAwwkHwwkSM3","kAwwkHwwkSM4","kAwwkHwwkSM5","kAwwkHwwkSM6","kAwwkHwwkSM7","kAwwkHwwkSM8","kAwwkHwwkSM9","kSM0"};
  // these are the validation samples: "v0","v1","v2","v3","v4","v5","v6","v7","v8","v9"

  // push all the input samples in a RooArgList
  RooArgList inputs;
  for(auto const& sample: samplelist) {
    RooStringVar* v = new RooStringVar(sample.c_str(),sample.c_str(),sample.c_str());
    inputs.add(*v);
  }

  // setup predefined morphfunc by hand
  RooLagrangianMorphFunc* morphfunc = new RooHCvbfWWMorphFunc(identifier.c_str(),identifier.c_str(),infilename.c_str(),observable.c_str(),inputs);
  if(!morphfunc->writeCoefficients("matrix.txt"))
    std::cout<<"failed to save matrix"<<std::endl;
  return 0;
}
