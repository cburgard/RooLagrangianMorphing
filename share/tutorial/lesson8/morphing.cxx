// morphing
#include "RooLagrangianMorphing/RooLagrangianMorphing.h"

// RooFit
#include "RooStringVar.h"

// ROOT
#include "TFile.h"
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "TFolder.h"
#include "TAxis.h"
#include "TLegend.h"
#include "TStyle.h"

int main(int argc, const char* argv[]){

  // define process identifier, input file and observable
  std::string identifier("ggfZZ"); // avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  std::string infilename("input/ggfhzz4l_2d.root"); // give the input file name here
  std::string observable("base/phi"); // name of the observable to be used (histogram name)

  // these are the names of the input samples
  std::vector<std::string> samplelist = {"s1","s2","s3"};
  // these are the validation samples: "v0","v1"

  // push all the input samples in a RooArgList
  RooArgList inputs;
  for(auto const& sample: samplelist) {
    RooStringVar* v = new RooStringVar(sample.c_str(),sample.c_str(),sample.c_str());
    inputs.add(*v);
  }

  // setup predefined morphfunc by hand
  RooLagrangianMorphFunc* morphfunc = new RooHCggfZZMorphFunc(identifier.c_str(),identifier.c_str(),infilename.c_str(),observable.c_str(),inputs);
  std::string standardmodel("s1");
  morphfunc->setParameters(standardmodel.c_str());

  // loop over the input samples
  double ymin = 0;
  double ymax = 0;
  std::vector<TGraphErrors*> graphs;
  for(auto s:samplelist){
    // retrieve the weight of the corresponding input sample
    RooAbsReal* w = morphfunc->getSampleWeight(s.c_str());
    TGraphErrors* g = new TGraphErrors();
    g->SetTitle(s.c_str());
    size_t i=0;
    // scan over a couple of points to fill the graph
    for(auto xval:{-5.,-4.,-3.,-2.,-1.,0.,1.,2.,3.,4.,5.}){
      morphfunc->setParameter("kAzz",xval);
      double yval = w->getVal();
      g->SetPoint(i,xval,yval);
      ymax = std::max(yval,ymax);
      ymin = std::min(yval,ymin);
      i++;
    }
    graphs.push_back(g);
  }

  // plot everything
  TCanvas* c = new TCanvas("plot");
  c->cd();
  TLegend* leg = new TLegend(0.7,0.7,0.9,0.9);
  int counter = 0;
  ymin = std::min(0.9*ymin,1.1*ymin);
  ymax = 1.1*ymax;
  gStyle->SetOptTitle(false);
  for(auto g:graphs){
    g->SetLineColor(counter+1);
    leg->AddEntry(g,g->GetTitle(),"l");
    if(counter==0){
      g->GetYaxis()->SetRangeUser(ymin,ymax);
      g->Draw("AL");
      g->GetXaxis()->SetTitle("#kappa_{AZZ}");
      g->GetYaxis()->SetTitle("sample weight");
    } else {
      g->Draw("L");
    }
    counter++;
  }
  leg->Draw();
  c->SaveAs("plot.pdf");
  return 0;
}
