#!/bin/env python
import argparse
import os

def main():
  import ROOT
  
  # define process identifier, input file and observable
  identifier = "ggfZZ" # avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  infilename = "input/ggfhzz4l_2d.root" # give the input file name here
  observable = "base/phi" # name of the observable to be used (histogram name)

  # these are the names of the input samples
  samplelist = ["s1","s2","s3"]
  # these are the validation samples: "v0","v1"

  # push all the input samples in a RooArgList
  inputs = ROOT.RooArgList()
  # we need the additional list "inputnames" to prevent the python garbage collector from deleting the RooStringVars
  inputnames = []
  for sample in samplelist:
    v = ROOT.RooStringVar(sample,sample,sample)
    inputnames.append(v)
    inputs.add(v)

  # setup predefined morphfunc by hand
  morphfunc = ROOT.RooHCggfZZMorphFunc(identifier,identifier,infilename,observable,inputs)

  standardmodel = "s1"
  morphfunc.setParameters(standardmodel)

  # loop over the input samples
  ymin = 0.
  ymax = 0.
  graphs = []
  for s in samplelist:
    # retrieve the weight of the corresponding input sample
    w = morphfunc.getSampleWeight(s)
    g = ROOT.TGraphErrors()
    g.SetTitle(s)
    i=0
    # scan over a couple of points to fill the graph
    for xval in [-5.,-4.,-3.,-2.,-1.,0.,1.,2.,3.,4.,5.]:
      morphfunc.setParameter("kAzz",xval)
      yval = w.getVal()
      g.SetPoint(i,xval,yval)
      ymax = max(yval,ymax)
      ymin = min(yval,ymin)
      i = i+1
    graphs.append(g)

  # plot everything
  c = ROOT.TCanvas("plot")
  c.cd()
  leg = ROOT.TLegend(0.7,0.7,0.9,0.9)
  counter = 0
  ymin = min(0.9*ymin,1.1*ymin)
  ymax = 1.1*ymax
  ROOT.gStyle.SetOptTitle(False)
  for g in graphs:
    g.SetLineColor(counter+1)
    leg.AddEntry(g,g.GetTitle(),"l")
    if counter==0:
      g.GetYaxis().SetRangeUser(ymin,ymax)
      g.Draw("AL")
      g.GetXaxis().SetTitle("#kappa_{AZZ}")
      g.GetYaxis().SetTitle("sample weight")
    else:
      g.Draw("L")
    counter = counter+1
  leg.Draw()
  c.SaveAs("plot.pdf")

if __name__ == "__main__":
  # some argument parsing to provide the path to the RooLagrangianMorphFunc
  parser = argparse.ArgumentParser("morphing example")
  args = parser.parse_args()
  # load all required libraries
  from ROOT import gSystem
  gSystem.Load("libRooFit")  
  gSystem.Load("libRooLagrangianMorphing.so")
  # call the main function
  main()
