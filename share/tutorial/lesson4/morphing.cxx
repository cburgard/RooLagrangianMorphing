// morphing
#include "RooLagrangianMorphing/RooLagrangianMorphing.h"

// RooFit
#include "RooStringVar.h"
#include "RooDataHist.h"

// ROOT
#include "TFile.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TFolder.h"
#include "TLegend.h"
#include "TStyle.h"

int main(int argc, const char* argv[]){

  // define process identifier, input file and observable
  std::string identifier("vbfWW"); // avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  std::string infilename("input/vbfhwwlvlv_3d.root"); // give the input file name here
  std::string observable("twoSelJets/dphijj"); // name of the observable to be used (histogram name)

  // these are the names of the input samples
  std::vector<std::string> samplelist = {"kAwwkHwwkSM0","kAwwkHwwkSM1","kAwwkHwwkSM10","","kAwwkHwwkSM11","kAwwkHwwkSM12","kAwwkHwwkSM13","kAwwkHwwkSM2","kAwwkHwwkSM3","kAwwkHwwkSM4","kAwwkHwwkSM5","kAwwkHwwkSM6","kAwwkHwwkSM7","kAwwkHwwkSM8","kAwwkHwwkSM9","kSM0"};
  // these are the validation samples: "v0","v1","v2","v3","v4","v5","v6","v7","v8","v9"

  // push all the input samples in a RooArgList
  RooArgList inputs;
  for(auto const& sample: samplelist) {
    RooStringVar* v = new RooStringVar(sample.c_str(),sample.c_str(),sample.c_str());
    inputs.add(*v);
  }

  // setup predefined morphfunc by hand
  RooLagrangianMorphPdf* morphfunc = new RooHCvbfWWMorphPdf(identifier.c_str(),identifier.c_str(),infilename.c_str(),observable.c_str(),inputs);

  // morph to the validation sample v1
  std::string validationsample("v1");
  morphfunc->setParameters(validationsample.c_str());
  TH1* morphing = morphfunc->createTH1("morphing");

  // open the input file to get the validation histogram for comparison
  TFile* file = TFile::Open(infilename.c_str(),"READ");
  TFolder* folder = 0;
  file->GetObject(validationsample.c_str(),folder);
  TH1* validation = dynamic_cast<TH1*>(folder->FindObject(observable.c_str()));
  validation->SetDirectory(NULL);
  validation->SetTitle(validationsample.c_str());
  file->Close();

  // setup the fit
  RooDataHist* target = RooLagrangianMorphing::makeDataHistogram(validation,morphfunc->getObservable(),"validation"); // convert the target to a RooDataHist
  morphfunc->setParameters(validationsample.c_str());
  morphfunc->setParameterConstant("Lambda",true);
  morphfunc->setParameterConstant("cosa",true);
  morphfunc->randomizeParameters(2); // randomize the parameters by 2 standard deviations to give the fit something to do
  morphfunc->printParameters();
  morphfunc->getPdf()->fitTo(*target,RooFit::SumW2Error(true),RooFit::Optimize(false)); // run the fit
  morphfunc->printParameters();
  TH1* fitresult = morphfunc->createTH1("fit result");

  // plot everything
  TCanvas* plot = new TCanvas("plot");
  plot->cd();
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  morphing->GetXaxis()->SetTitle(observable.c_str());
  morphing->SetLineColor(kRed);
  morphing->SetFillColor(kRed);
  morphing->Draw("E3");
  fitresult->SetLineColor(kBlack);
  fitresult->SetMarkerColor(kBlack);
  fitresult->SetMarkerStyle(20);
  fitresult->Draw("PSAME");
  validation->Draw("SAME");
  TLegend* leg = new TLegend(0.7,0.7,0.9,0.9);
  leg->AddEntry(fitresult);
  leg->AddEntry(morphing);
  leg->AddEntry(validation);
  leg->Draw();
  plot->SaveAs("plot.pdf","pdf");
  return 0;
}
